FROM node:10-alpine

RUN apk update && apk add --no-cache ca-certificates curl
ADD pipe .
COPY .npmrc .npmrc
RUN npm set @atlassiansox:registry https://packages.atlassian.com/api/npm/atlassian-npm/
RUN npm i
RUN rm .npmrc
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
