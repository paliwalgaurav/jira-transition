const nock = require('nock')
const requireDirectory = require('require-directory')

const { responses } = requireDirectory(module)
const baseUrl = 'https://example.com'

function getIssue (onReply, issueKey = responses.getIssue.key) {
  return nock(baseUrl)
    .persist()
    .get(`/rest/api/2/issue/${issueKey}`)
    .query(true)
    .reply(200, onReply(responses.getIssue))
}

function getMyself (onReply) {
  return nock(baseUrl)
    .get(`/rest/api/3/myself`)
    .query(true)
    .reply(200, onReply(responses.getMyself))
}

function getIssueTransitions (onReply, issueKey = responses.getIssue.key) {
  return nock(baseUrl)
    .get(`/rest/api/2/issue/${issueKey}/transitions`)
    .reply(200, onReply(responses.getIssueTransitions))
}

function transitionIssue (onReply, issueKey = responses.getIssue.key) {
  return nock(baseUrl)
    .post(`/rest/api/3/issue/${issueKey}/transitions`)
    .query(true)
    .reply(200, onReply({}))
}

module.exports = {
  getIssue,
  getMyself,
  getIssueTransitions,
  transitionIssue,
}
