const { analyticsClient } = require('@atlassiansox/analytics-node-client')
const debug = require('debug')('pipe')
const pckg = require('../package.json')

const behavioralAnalyticsClient = analyticsClient({
  env: process.env.NODE_ENV === 'testing' ? 'stg' : 'prod',
  product: 'externalProductIntegrations',
  subproduct: 'bbPipeJiraTransition',
  version: pckg.version,
})

async function sendTrackEvent (event) {
  const payload = {
    ...event,
    userIdType: event.userId ? 'atlassianAccount' : undefined,
    userId: event.userId,
    anonymousId: process.env.BITBUCKET_REPO_UUID || 'unknown',
    trackEvent: {
      origin: 'console',
      platform: 'bot',
      source: 'bitbucket',
      containerType: 'repository',
      containerId: process.env.BITBUCKET_REPO_UUID,
      ...event.trackEvent,
    },
  }

  debug(`Sending analytics: ${JSON.stringify(payload, null, 4)}`)

  if (process.env.NODE_ENV === 'testing') {
    return
  }

  try {
    await behavioralAnalyticsClient.sendTrackEvent(payload)
  } catch (error) {
    console.error(`Failed sending analytics:`)
    console.error(error)
  }
}

module.exports = {
  sendTrackEvent,
}
