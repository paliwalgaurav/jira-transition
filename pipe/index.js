const debug = require('debug')('pipe')
const url = require('url')
const Pipe = require('./pipe')
const Jira = require('./common/net/Jira')

async function exec ({
  JIRA_BASE_URL,
  JIRA_API_TOKEN,
  JIRA_USER_EMAIL,
  TRANSITION,
  ISSUE,
} = {}) {
  if (!JIRA_BASE_URL) throw new Error('Please specify JIRA_BASE_URL')
  const parsedUrl = new url.URL(JIRA_BASE_URL)
  const baseUrl = `${parsedUrl.protocol}//${parsedUrl.hostname}`

  debug(`Parsed baseUrl:${baseUrl}`)

  if (!JIRA_API_TOKEN) throw new Error('Please specify JIRA_API_TOKEN')
  if (!JIRA_USER_EMAIL) throw new Error('Please specify JIRA_USER_EMAIL')
  if (!TRANSITION) throw new Error('Please specify TRANSITION')
  if (!ISSUE) throw new Error('Please specify ISSUE')

  const result = await new Pipe({
    Jira: new Jira({
      baseUrl,
      token: JIRA_API_TOKEN,
      email: JIRA_USER_EMAIL,
    }),
  }).execute({
    transitionName: TRANSITION,
    issueKeyString: ISSUE,
  })

  return result
}

async function shellExec () {
  try {
    await exec(process.env)
    process.exit(0)
  } catch (error) {
    console.error(error.message || error)
    process.exit(1)
  }
}

if (!module.parent) {
  shellExec()
}

module.exports = exec
